//
//  main.swift
//  Tsaryov-Third-Task
//
//  Created by Ivan Tsaryov on 01/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

func printRepositories(_ array: [Repository]) {
    for repository in array {
        print(repository.name)
    }
}

func getUserRepositories() {
    print("Enter the username:")
    
    guard let username = readLine() else {
        print("Wrong username")
        return getUserRepositories()
    }
    
    print("Requesting repositories for: \"\(username)\"..")
    Alamofire.request("https://api.github.com/users/\(username)/repos", method: .get)
        .responseArray { (response: DataResponse<[Repository]>) in
            response.result.ifSuccess {
                if let repositories = response.result.value {
                    print("User: \"\(username)\" has following repositories:")
                    printRepositories(repositories)
                }
            }
            
            response.result.ifFailure {
                print("Failure")
            }
    }
}

getUserRepositories()

RunLoop.main.run()
