//
//  Repository.swift
//  Tsaryov-Third-Task
//
//  Created by Ivan Tsaryov on 01/01/2018.
//  Copyright © 2018 Ivan Tsaryov. All rights reserved.
//

import ObjectMapper


struct Repository {
    
    var name: Any = "undefined"
}

extension Repository: Mappable {
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        name <- map["name"]
    }
}
